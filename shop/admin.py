# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from shop.models import Category, Product
from django.contrib import admin

# Register your models here.

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'catSlug':('catName',)}
    list_display = ('catName', 'catSlug')

class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {'prodName': ('prodSlug',)}
    list_display = ('prodName','prodSlug','price', 'stock', 'availability', 'created', 'updated')


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)