# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import Category, Product
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
def product_list(request, catSlug=None):
    prodFlag = False
    try:
        category = Category.objects.get(catSlug=catSlug)
    except  ObjectDoesNotExist:
        category = None
        prodFlag = True
    categories = Category.objects.all()
    if prodFlag == False:
        products = Product.objects.filter(category=category)
    else:
        products = Product.objects.all()
    print products
    context_dict = {'category': category,'categories': categories, 'products': products}

    return render(request, 'shop/product_list.html', context_dict)

def detailProduct(request, id, prodSlug):

    product = Product.objects.get(id = id)
    quantity = range(product.stock)

    context_dict = {'product': product, 'quantity': quantity}

    return render(request, 'shop/detail.html', context_dict)


def base(request):
    product_list = Product.objects.order_by("-id")
    category_list = Category.objects.order_by("-id")
    context_dict = {'products': product_list, 'categories': category_list}

    return render(request, 'base.html', context_dict)