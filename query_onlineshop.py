import os, django
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'onlineshop.settings')
django.setup()
from shop.models import Product, Category
from django.core.files import File
from onlineshop.settings import IMAGES_URL
from django.core.exceptions import ObjectDoesNotExist

c1 = Category.objects.get_or_create(catName="ofertas")[0]
c2 = Category.objects.get_or_create(catName="gangas")[0]

c1.save()
c2.save()

p1 = Product.objects.get_or_create(category= c1, prodName="oferta 1", description="", price=1, stock=15)[0]
p2 = Product.objects.get_or_create(category= c1, prodName="oferta 2", description="", price=1, stock=15)[0]
p3 = Product.objects.get_or_create(category= c1, prodName="oferta 3", description="", price=1, stock=15)[0]

imageObject = File(open(os.path.join(IMAGES_URL, "607.jpg"), 'r'))
p1.image.save("oferta1.jpg", imageObject, save=True)
p1.image.save("oferta2.jpg", imageObject, save=True)
p1.image.save("oferta3.jpg", imageObject, save=True)

g1 = Product.objects.get_or_create(category= c2, prodName="ganga 1", description="", price=1, stock=15)[0]
g2 = Product.objects.get_or_create(category= c2, prodName="ganga 2", description="", price=1, stock=15)[0]
g3 = Product.objects.get_or_create(category= c2, prodName="ganga 3", description="", price=1, stock=15)[0]

imageObject = File(open(os.path.join(IMAGES_URL, "607.jpg"), 'r'))
p1.image.save("ganga1.jpg", imageObject, save=True)
p1.image.save("ganga2.jpg", imageObject, save=True)
p1.image.save("ganga3.jpg", imageObject, save=True)

q1 = Product.objects.filter(category=c2)

print q1

q2 = Product.objects.get(prodSlug="oferta-1")

q3 = q2.category

print q3.catSlug

nombre = "oferta_10"

try :
    q4 = Product.objects.get(prodName = nombre)
except  ObjectDoesNotExist :
    print "producto " + nombre + " inexistente"




