# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils import timezone
import datetime

from django.template.defaultfilters import slugify

# Create your models here.

class Category(models.Model):
    catName = models.CharField(max_length=128, unique=True)
    catSlug = models.SlugField(unique=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def save(self, *args, **kwargs):
         self.catSlug = slugify(self.catName)
         super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.catName

class Product(models.Model):
    category = models.ForeignKey(Category)
    prodName = models.CharField(max_length=128, unique=True)
    prodSlug = models.SlugField(unique=True)
    image = models.ImageField(upload_to = 'media/images/products')
    description = models.CharField(max_length=256, unique=False)
    price = models.DecimalField(max_digits=8, decimal_places=2, default=10.00)
    stock = models.IntegerField(default=1)
    availability = models.BooleanField(default=True)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        self.prodSlug = slugify(self.prodName)
        super(Product, self).save(*args, **kwargs)

    def __str__(self):  # For Python 2, use __unicode__ too
        return self.prodName