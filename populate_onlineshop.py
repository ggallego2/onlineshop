import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                       'onlineshop.settings')
import django
django.setup()
from shop.models import Category, Product
from django.core.files import File
from onlineshop.settings import IMAGES_URL


def populate():

    Hombre = [
        {"proName": "O'de Home", "description": "it smeels like Homer", "price": 11.30, "stock": 14,
         "image": "odehome.png"},
        {"proName": "O'de Chat", "description": "it smeels like Chat", "price": 4, "stock": 3,
         "image": "odechat.jpeg"},
        {"proName": "O'de MergeSort", "description": "No funciona Mergesort", "price": 15.30, "stock": 20,
         "image": "odemergesort.jpg"},
        {"proName": "O'de Campo", "description": "It smells like my grandma", "price": 1.30, "stock": 24,
         "image": "odecampo.jpg"},
        {"proName": "O'de POWER", "description": "P P P P P P POWER", "price": 25.30, "stock": 1,
         "image": "odepower.jpg"},
        {"proName": "O'de ende", "description": "The last one, at last", "price": 5.30, "stock": 13,
         "image": "odend.jpg"},
    ]
    Mujer = [
        {"proName": "O'de Coquille", "description": "it smeels like fish", "price": 10, "stock": 10,
         "image": "odesea.JPG"},
        {"proName": "O'de Marge", "description": "it smeels like Marge", "price": 11.30, "stock": 14,
         "image": "odemarge.png"},
        {"proName": "O'de Lisa", "description": "Que no lisa?, que no?", "price": 11.30, "stock": 14,
         "image": "odelisa.jpg"},
        {"proName": "O'de Perfume", "description": "Perfume", "price": 15.30, "stock": 20,
         "image": "odeperfume.jpg"},
        {"proName": "O'de CIREL", "description": "No es SOPER", "price": 10.55, "stock": 4,
         "image": "odecirel.jpeg"},
        {"proName": "O'de acaba", "description": "Let it end", "price": 12.15, "stock": 8,
         "image": "odejoy.jpg"},
    ]
    Unisex = [
        {"proName": "O'de Fairy", "description": "it smeels like clean", "price": 14.30, "stock": 14,
         "image": "odefairy.jpeg"},
        {"proName": "CK BE", "description": "Calvin Klein unisex", "price": 21.40, "stock": 17,
         "image": "ckbe.jpg"},
        {"proName": "O'de GOOD", "description": "it smeels pretty good", "price": 9.90, "stock": 9,
         "image": "607.jpg"},
        {"proName": "O'de Later", "description": "Smell ya later", "price": 16.35, "stock": 21,
         "image": "pizza.png"},
        {"proName": "O'de Toilette", "description": "Toilette", "price": 0.95, "stock": 37,
         "image": "odetoilette.jpg"},
        {"proName": "O'de END", "description": "Its finally over", "price": 15, "stock": 18,
         "image": "odeale.jpeg"},
    ]



    cats = {"Hombre" : Hombre, "Mujer" : Mujer, "Unisex" : Unisex}



    for cat, cat_data in cats.items():
        c = add_cat(cat)
        for p in cat_data:
            add_product(c, p["proName"], p["description"], p["price"], p["stock"], p["image"])

    # Print out the categories we have added.
    for c in Category.objects.all():
        for p in Product.objects.filter(category=c):
            print("- {0} - {1}".format(str(c), str(p)))

def add_product(cat, name, desc, pricen, stock, image):
     p = Product.objects.get_or_create(category=cat, prodName=name, description=desc, price=pricen, stock=stock)[0]
     imageObject = File(open(os.path.join(IMAGES_URL, image), 'r'))
     p.image.save(image, imageObject, save=True)
     return p

def add_cat(name):
     c = Category.objects.get_or_create(catName=name)[0]
     c.save()
     return c
# Start execution here!
if __name__ == '__main__':
     print("Starting Rango population script...")
     populate()
